# Changelog for Coolfire Offset

## 0.7.0 - Third beta - 2014-9-11

- Introduces Bower components via CodeKit
- Sublime Text project files
- Simplifies and clarifies project structure
- HTML5 captions
- New Sass mixins for breakpoint, clearfix
- CSS resets
- Formats scaffolding for TinyMCE
- Removes default meta
- Removes old pagination
- More robust reset
- Removes classes from main element

## 0.6.0 - Second beta - 2014-03-24

- Stripped base wrapper and added those calls to templates
- Stripped sidebar logic

## 0.5.0 - First beta - 2014-03-23 - Forked from Offset 0.3.0

- Removes Grunt tasks that we won't use
- Removes Bootstrap components

## 0.3.0 - Third alpha - 2014-03-07

- Adds theme wrapper
- Establishes an array where you can specify all templates without a sidebar

## 0.2.3 - 2014-03-03

- Re-added Navwalker as a subtree due to problems including Offset in projects

## 0.2.2 - 2014-02-27

- Theme support for HTML5 galleries

## 0.2.1 - 2014-02-27

- Tried adding Navwalker as a submodule (turned out to be a bad idea)

## 0.2.0 - Second alpha - 2014-02-25

- convert theme dev to Bootstrap due to Inuit bugs
- adds the Bootstrap dropdown JS to the main nav
- stepping back from a few custom choices
- PHPDoc blocks for all major files - this is my first legit usage of PHPDoc so please submit pulls if I screwed up anywhere.

## 0.1.0 - First alpha - 2014-02-19

- All major theme parts done and glaring, screaming bugs fixed
