# Coolfire Offset

Coolfire Offset is a WordPress starter kit customized for Coolfire Studios.

## Credits

Offset Printer Photo copyright [Nadja Robot](https://secure.flickr.com/photos/nadja_robot/2490910807/)
Thanks to [_s](http://underscores.me/) and [Bones](http://themble.com/bones/) for inspiration and a few code snippets.
Thanks to [Roots Team](http://roots.io/) for lots of code and understanding of a better way to manage a WordPress stack
