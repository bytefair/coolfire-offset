<?php
/**
 * footer.php
 *
 * Site footer
 *
 * @package Offset\Templates
 * @author Paul Graham <paul@bytefair.com>
 * @license http://opensource.org/licenses/MIT
 * @since 0.1.0
 */
?>

</div>
</div>

<div class="footer__wrapper">
<footer role="contentinfo" class="site__footer">
	<div class="site__footer__content">
	</div>
</footer>
</div>


</div><!-- end of .site__wrapper -->
<?php wp_footer(); ?>
</body>
</html>
