# fixes needed

1. show # comments on blog/archive view
2. bad "categorized" shows up on pages in search
3. make shortcodes for columns
4. make shortcodes for buttons
5. must show comments on pages if people are dumb, check "page with comments" on theme unit test
6. fix the thumbnails, inheriting problems from default thumbnail class
